<?php include("crud.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Herugroupschool</title>
    <link rel="shortcut icon" href="herugroups.png">
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
    <header>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<a class="navbar-brand" href="#">
		<img src="herugroups.png" width="65" class="d-inlin-block align-top" alt="" loading="lazy">
	</a>
<span class="navbar-brand mb-0 h1">Herugroupschool</span>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Sejarah</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lainnya
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cetak.php">Cetak Data</a>
          <a class="dropdown-item" href="list-siswa.php">Data Siswa</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="logout.php">Keluar</a>
        </div>
      </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Coming soon</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="nav-link disabled" type="search" placeholder="Search" aria-label="Search">
      <button class="nav-link disabled" type="submit">Search</button>
    </form>
  </div>
</nav>
    <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                <nav class="nav flex-sidebar">
                    <a class="nav-link active" href="index.php"><h5>HOME</h5></a>
                    <a class="nav-link" href="cetak.php"><h5>CETAK DATA</h5></a>
                    <a class="nav-link" href="list-siswa.php"><h5>SEMUA DATA</h5></a>
                    <a class="nav-link" href="logout.php"><h5>KELUAR</h5></a>
                </nav>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1>Daftar siswa yang sudah terdaftar</h1>
                    <?php include("crud.php");?>
                    
                    <table class="table table-striped text-center">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Jurusan</th>
                                <th>Jenis Kelamin</th>
                                <th>Email</th>
                                <th>No Handphone</th>
                            </tr>
                        </thead>
                        <?php
                            $sql ="SELECT * FROM calon_siswa";
                            $query =mysqli_query($db, $sql);
                            $no=0;
                            while($siswa=mysqli_fetch_array($query)){
                                $no++;
                                echo "<tr>";
                                echo "<td>".$no."</td>";
                                echo "<td>".$siswa['nama']."</td>";
                                echo "<td>".$siswa['tempat_lahir']."</td>";
                                echo "<td>".$siswa['tanggal_lahir']."</td>";
                                echo "<td>".$siswa['jurusan']."</td>";
                                echo "<td>".$siswa['jenis_kelamin']."</td>";
                                echo "<td>".$siswa['email']."</td>";
                                echo "<td>".$siswa['no_handphone']."</td>";
                                // echo "<td>";
                                echo "</td>";
                            }
                        ?>
                    </tbody>
                    <p>Total: <?php echo mysqli_num_rows($query)?></p>
                </div>
            </div>
        </div>
  </body>
</html>