<!DOCTYPE html>
<html>
<head>
    <title>Herugroupschool</title>
    <link rel="shortcut icon" href="herugroups.png">
    <link href="dashboard.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>     
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<a class="navbar-brand" href="#">
		<img src="herugroups.png" width="65" class="d-inlin-block align-top" alt="" loading="lazy">
	</a>
<span class="navbar-brand mb-0 h1">Herugroupschool</span>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Sejarah</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lainnya
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cetak.php">Cetak Data</a>
          <a class="dropdown-item" href="list-siswa.php">Data Siswa</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="logout.php">Keluar</a>
        </div>
      </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Coming soon</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="nav-link disabled" type="search" placeholder="Search" aria-label="Search">
      <button class="nav-link disabled" type="submit">Search</button>
    </form>
  </div>
</nav>
<div class="container-fluid">
    <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                <nav class="nav flex-sidebar">
                    <a class="nav-link active" href="index.php"><h5>HOME</h5></a>
                    <a class="nav-link" href="cetak.php"><h5>CETAK DATA</h5></a>
                    <a class="nav-link" href="list-siswa.php"><h5>SEMUA DATA</h5></a>
                    <a class="nav-link" href="logout.php"><h5>KELUAR</h5></a>
                </nav>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <center><h3>HERUGROUPSCHOOL<br>Form Pendaftaran</h3></center><br>
    <form class="needs-validation" action="proses-pendaftaran.php" method="POST" id="form" required>
        <div class="form-group">
            <label for="nama">Nama : </label><br>
            <input type="text" name="nama" placeholder="Nama lengkap" class="form-control" id="" aria-describedby="emailHelp" required/>
          <div class="valid-feedback">Looks good!</div>
        </div>
        <div class="form-group">
            <label for="tempat_lahir">Tempat Lahir : </label>
            <input type="text" name="tempat_lahir" placeholder="Tempat lahir" class="form-control" id="" required/>
        </div>
        <div class="form-group">
            <label for="tanggal_lahir">Tanggal Lahir : </label>
            <input type="text" name="tanggal_lahir" placeholder="Tahun-bulan-tanggal" class="form-control" id="" required/>
        </div>
        <div class="form-group">
                <label for="jurusan">Jurusan :</label><br>
                <select class="form-control" name="jurusan" id="" required>
                <option value="">Pilih Jurusan</option>
                <option value="Web Developer">Web Developer</option>
                <option value="Desain">Desain</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                </select>
        </div>
        <div class="form-group">
            <label for="jenis_kelamin">Jenis Kelamin  : </label><br>
            <select class="form-control" name="jenis_kelamin" id="" required>
            <option value="">Jenis Kelamin anda</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="email">Email  : </label>
            <input type="text" name="email" placeholder="email" class="form-control" id="" required/>
        </div>
        <div class="form-group">
            <label for="no_handphone">No Handphone : </label>
            <input type="number" name="no_handphone" onekeypress="return hanyaAngka(event)" placeholder="No handphone" class="form-control" id="" required/>
        </div>
        <a href="index.php" class="btn btn-warning"> Kembali</a>
            <!-- <input type="submit" value="Daftar" name="daftar" /> -->
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="submit" value="Daftar" class="btn btn-primary" name="daftar">Daftar</button>
        
    </form>
    </body>
    </div>
</div>
</html>
